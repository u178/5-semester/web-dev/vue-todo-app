# todo-app

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

![](/home/gleb/.var/app/com.github.marktext.marktext/config/marktext/images/2021-12-23-09-38-14-image.png)

![](/home/gleb/.var/app/com.github.marktext.marktext/config/marktext/images/2021-12-23-09-39-03-image.png)

![](/home/gleb/.var/app/com.github.marktext.marktext/config/marktext/images/2021-12-23-09-39-48-image.png)
